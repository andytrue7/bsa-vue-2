import axios from 'axios'

export default {
    namespaced: true,

    actions: {
       async fetchAlbums({ commit }) {
           const res = await axios.get('https://jsonplaceholder.typicode.com/albums');
            
            commit('setAlbums', res.data);
        },

        
        async addAlbum({commit}, {title, userId, preview}) {
            const res =  await axios.post('https://jsonplaceholder.typicode.com/albums', {
                    title,
                    userId,
                    preview
                
                });

            commit('createAlbum', res.data);
        },

        async deleteAlbum({commit}, id) {
            await axios.delete('https://jsonplaceholder.typicode.com/albums/'+id);

            commit('removeAlbums', id);
        }


            
    },

    mutations: {
        setAlbums(state, albums) {
            state.albums = albums;
        },

        createAlbum(state, newAlbum) {
            state.albums.push(newAlbum); 
        },

        removeAlbums: (state, id) => (state.albums = state.albums.filter(album => album.id !== id ))
    },

    state: {
        albums: [],
    },

    getters: {
        allAlbums(state) {
            return state.albums;
        },    
    },
}