import axios from 'axios'

export default {
    namespaced: true,

    actions: {
       
        async fetchPhotosByAlbumId({ commit }, id) {
            const res = await axios.get('https://jsonplaceholder.typicode.com/photos?albumId='+id);

            commit ('setPhotos', res.data);

        },
    
    },

    mutations: {
        setPhotos(state, photos) {
            state.photos = photos;
        },
    },

    state: {
        photos: [],
    },

    getters: {
        allPhotos(state) {
            return state.photos;
        }, 
    },
}