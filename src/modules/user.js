import axios from 'axios'

export default {
    namespaced: true,

    actions: {
       async fetchUsers({ commit }) {
           const res = await axios.get('https://jsonplaceholder.typicode.com/users');
            
            commit('setUsers', res.data);
        },

        async fetchUserById({ commit }, id) {
            const res = await axios.get('https://jsonplaceholder.typicode.com/users/'+id);

            commit ('setUser', res.data);

        },

        async addUser({commit}, {name, email, avatar}) {
            const res =  await axios.post('https://jsonplaceholder.typicode.com/users', {
                    name,
                    email,
                    avatar
                
                });

            commit('createUser', res.data);
        },

        async updateUser({commit}, {id, name, email, avatar}) {
            const res =  await axios.patch('https://jsonplaceholder.typicode.com/users'+id, {
                    name,
                    email,
                    avatar
                
                });

            commit('setUser', res.data);
        },

        async deleteUser({commit}, id) {
            await axios.delete('https://jsonplaceholder.typicode.com/users/'+id);

            commit('removeUser', id);
        }


            
    },

    mutations: {
        setUsers(state, users) {
            state.users = users;
        },

        setUser(state, user) {
            state.user = user;
        },

        createUser(state, newUser) {
            state.users.push(newUser); 
        },

        removeUser: (state, id) => (state.users = state.users.filter(user => user.id !== id ))
    },

    state: {
        users: [],
        user: {}
        
    },

    getters: {
        allUsers(state) {
            return state.users;
        },

        getUserById: (state) => (id) => {
            return state.users.find(user => user.id === id);
        },

        filteredByNameUsers: (state) => (searchKey) => {
            
            if(!searchKey) {
                return state.users;
              }
              return state.users.filter(user => user.name.indexOf(searchKey) !== -1);
        },   
    
    },
}