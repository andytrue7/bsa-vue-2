import Vue from 'vue'
import VueRouter from 'vue-router'
import User from '@/views/User'
import AddUserForm from '@/components/users/AddUserForm'
import UserPage from '@/components/users/UserPage'
import EditUserForm from '@/components/users/EditUserForm'
import Album from '@/views/Album'
import AlbumPage from '@/components/albums/AlbumPage'
import AddAlbumForm from '@/components/albums/AddAlbumForm'
import EditAlbumForm from '@/components/albums/EditAlbumForm'

Vue.use(VueRouter)

  const routes = [
    {
      path: '/users',
      component: User
    },


    {
      path: '/users/add',
      component: AddUserForm
    },

    {
      path: '/users/:id',
      component: UserPage,
      name: 'user',
    },

    {
      path: '/users/:id/edit',
      component: EditUserForm,
      name: 'user-edit',
    },

    {
      path: '/albums',
      component: Album
    },

    {
      path: '/albums/add',
      component: AddAlbumForm
    },

    {
      path: '/albums/:id',
      component: AlbumPage,
      name: 'album',
    },

    {
      path: '/albums/:id/edit',
      component: EditAlbumForm,
      name: 'album-edit',
    },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
